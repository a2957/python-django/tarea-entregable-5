from rest_framework.serializers import ModelSerializer

from students.serializers import StudentSerializer
from teachers.serializers import TeacherSerializer
from .models import Class


class ClassSerializer(ModelSerializer):

    class Meta:
        model = Class
        fields = '__all__'


class ClassDetailSerializer(ModelSerializer):
    teacher = TeacherSerializer(read_only=True)
    students = StudentSerializer(many=True, read_only=True)

    class Meta:
        model = Class
        fields = '__all__'
