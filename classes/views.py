from rest_framework.viewsets import ModelViewSet

from classes.models import Class
from classes.serializers import ClassSerializer, ClassDetailSerializer


class ClassViewSet(ModelViewSet):
    queryset = Class.objects.all()
    serializer_class = ClassSerializer

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return ClassDetailSerializer

        return self.serializer_class
