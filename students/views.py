# Create your views here.
from rest_framework.viewsets import ModelViewSet

from students.permissions import CustomStudentPermissions
from students.models import Student
from students.serializers import StudentSerializer, StudentDetailSerializer


class StudentViewSet(ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    permission_classes = (CustomStudentPermissions, )

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return StudentDetailSerializer

        return self.serializer_class
