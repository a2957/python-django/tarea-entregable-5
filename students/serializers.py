from rest_framework.serializers import ModelSerializer

from accounts.serializers import UserSerializer
from students.models import Student


class StudentSerializer(ModelSerializer):

    class Meta:
        model = Student
        fields = '__all__'


class StudentDetailSerializer(ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Student
        fields = '__all__'
