from copy import copy

from django.core.mail import send_mail
from rest_framework.viewsets import ModelViewSet
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework import status

from students.models import Student
from students.serializers import StudentDetailSerializer
from teachers.models import Teacher
from teachers.serializers import TeacherDetailSerializer
from .permissions import CustomPermissions
from .serializers import UserSerializer, AdminRegisterSerializer, TeacherRegisterSerializer, StudentRegisterSerializer
from rest_framework.decorators import action


class AccountsViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (CustomPermissions,)

    @action(methods=['POST'], detail=False)
    def register(self, request):
        data = copy(request.data)
        del data['type']
        user = None

        if request.data['type'] == 'admin':
            new_user = AdminRegisterSerializer(data=data)
            new_user.is_valid(raise_exception=True)
            user = new_user.save()

        if request.data['type'] == 'teacher':
            new_user = TeacherRegisterSerializer(data=data)
            new_user.is_valid(raise_exception=True)
            user = new_user.save()

        if request.data['type'] == 'student':
            new_user = StudentRegisterSerializer(data=data)
            new_user.is_valid(raise_exception=True)
            user = new_user.save()

        send_mail(
            subject='Bienvenido al Institute',
            message=f'Su usuario a sido creado con éxito: username: {user.username}',
            from_email='info@institute.com',
            recipient_list=[user.email],
            html_message=f'''
            <body>
                <h1>Bienvenido al Institute</h1>
                <p>Su usuario ha sido creado con éxito.</p>
                <ul>
                    <li><b>Username: </b>{user.username}</li>
                </ul>
            </body>
            '''
        )
        return Response(
            status=status.HTTP_201_CREATED,
            data={
                "user": UserSerializer(user, context=self.get_serializer_context()).data
            }
        )

    @action(methods=['GET'], detail=False)
    def profile(self, request):

        if request.user.is_superuser:
            teachers = Teacher.objects.all()
            return Response(
                status=status.HTTP_200_OK,
                data={
                    "teachers": TeacherDetailSerializer(teachers, many=True).data
                }
            )

        if request.user.is_staff:
            profile = Teacher.objects.get(user=request.user)
            return Response(
                status=status.HTTP_200_OK,
                data={
                    "profile": TeacherDetailSerializer(profile).data
                }
            )

        if not request.user.is_staff:
            profile = Student.objects.get(user=request.user)
            return Response(
                status=status.HTTP_200_OK,
                data={
                    "profile": StudentDetailSerializer(profile).data
                }
            )

        return Response(
            status=status.HTTP_204_NO_CONTENT
        )
