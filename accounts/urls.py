from rest_framework.routers import DefaultRouter
from .views import AccountsViewSet


router = DefaultRouter()
router.register('accounts', AccountsViewSet)

urlpatterns = router.urls
