from rest_framework.permissions import BasePermission


class CustomPermissions(BasePermission):

    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True

        if view.action == 'register':
            return True

        if request.user.is_authenticated and view.action == 'profile':
            return True

        return False
