from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from classes.serializers import ClassDetailSerializer
from teachers.models import Teacher
from teachers.permissions import CustomTeacherPermissions
from teachers.serializers import TeacherSerializer, TeacherDetailSerializer


class TeacherViewSet(ModelViewSet):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer
    permission_classes = (CustomTeacherPermissions,)

    def retrieve(self, request, *args, **kwargs):
        teacher = self.get_object()
        return Response(
            status=status.HTTP_200_OK,
            data=TeacherDetailSerializer(teacher).data,
        )

    @action(methods=['GET'], detail=True)
    def classes(self, request, pk=None):
        teacher = self.get_object()
        classes = teacher.classes.all()

        return Response(
            status=status.HTTP_200_OK,
            data=ClassDetailSerializer(classes, many=True).data
        )
