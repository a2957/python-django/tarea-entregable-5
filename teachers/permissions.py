from rest_framework.permissions import BasePermission


class CustomTeacherPermissions(BasePermission):

    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        if request.user.is_staff and request.method == 'GET':
            return True

        return False

    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return True

        if request.user.is_staff and request.method == 'GET' and obj.user == request.user:
            return True

        return False
