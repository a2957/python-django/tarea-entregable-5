from django.contrib.auth.models import User
from django.db import models


class Teacher(models.Model):
    user = models.ForeignKey(User, related_name='teacher', on_delete=models.SET_NULL, null=True)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)

    def __str__(self) -> str:
        return f'{self.first_name} {self.last_name}'
