from rest_framework.serializers import ModelSerializer

from teachers.models import Teacher
from accounts.serializers import UserSerializer


class TeacherSerializer(ModelSerializer):

    class Meta:
        model = Teacher
        fields = '__all__'


class TeacherDetailSerializer(ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Teacher
        fields = '__all__'
